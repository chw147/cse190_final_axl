#!/usr/bin/env python

from read_config import read_config

import random,math

class qlearning():


  """
  Class for cell object
  """
  class Cell():
    def __init__(self, type, reward):
      self.type = type
      self.value = reward
      #QValue--- direction : qvalue
      #Direction: North, South, West, East
      self.QValues = {"N":0,"S":0,"W":0,"E":0}
      self.act_counts = {"N":1, "S":1, "W":1, "E":1}
    
    def get_QValue(self, action):
      return self.QValues[action]

    def set_QValue(self, action, qvalue):  
      self.QValues[action] = qvalue

    def get_actCount(self, action):
      return self.act_counts[action]

    def set_actCount(self, action, count):  
      self.act_counts[action] = count

    def maxQValue(self, weight):
      max_qval = 0
      for act in self.QValues:
        sum = weight/self.act_counts[act] + self.QValues[act]
        if sum > max_qval:
          max_qval = sum
      return max_qval

    def getAction(self,epsilon_val):
      action = None
      # Change to QValues to action list
      action_list = []
      for act in self.QValues:
        action_list.append(act)
      if random.uniform(0,1) < epsilon_val:
        action = random.choice(action_list)
      else:
        action = self.get_policy()
        if action is None:
          action = random.choice(action_list)
      self.act_counts[action] += 1 
      return action
    
    def set_value(self,reward):
      self.value = reward

    def get_value(self):
      return self.value

    def get_type(self):
      return self.type

    def get_policy(self):
      max_qval = None
      max_act = None
      for act in self.QValues:
        curr_qval = self.QValues[act]
        if max_qval < curr_qval or max_qval == None:
          max_qval = curr_qval
          max_act = act
      return max_act

    def update(self, action, next_cell, reward, learning_rate, discount_factor, weight):
      #self.N[self,action] += 1
      next_type = next_cell.get_type()
      if next_type == "goal" or next_type == "block":
        max_qval = next_cell.get_value()
      else:
        max_qval = next_cell.maxQValue(weight)
      curr_qval = self.get_QValue(action)
      new_qval = curr_qval + learning_rate*(reward + discount_factor*max_qval - curr_qval)
      self.QValues[action] = new_qval
  """
  Cell class ending
  """



  def __init__(self):
    self.config = read_config()
    
    # initialize variables
    #self.pos_goal = self.config["goal"]
    #self.pos_pits = self.config["pits"]
    #self.pos_walls = self.config["walls"]
    self.max_steps = self.config["max_iterations"]
    #self.threshold = self.config["threshold_difference"]
    self.rewards = {}
    self.rewards["step"] = self.config["reward_for_each_step"]
    self.rewards["wall"] = self.config["reward_for_hitting_wall"]
    self.rewards["goal"] = self.config["reward_for_reaching_goal"]
    self.rewards["block"] = self.config["reward_for_block"]
    self.epsilon_val = 0.8
    self.learning_rate = 0.3
    self.weight = 50 
    self.discount_factor = self.config["discount_factor"]
    #initialize a Counter (dict) object to map "cell,action" pair to 
    #the number of times our agent choose such an action in this state
    #self.N = util.Counter()

    # initialize grid
    self.num_row = self.config["map_size"][0]
    self.num_col = self.config["map_size"][1]
    self.grid = [[self.Cell("free", 0) for col in range(self.num_col)] for row in range(self.num_row)]
    # for i in range(self.num_row):
    #   for j in range(self.num_col):
    #     if [i, j] == self.pos_goal:
    #       self.grid[i][j] = self.Cell("goal", self.rewards["goal"])
    #     elif [i, j] in self.pos_pits:
    #       self.grid[i][j] = self.Cell("block", self.rewards["block"])
    #     elif [i, j] in self.pos_walls:
    #       self.grid[i][j] = self.Cell("wall", self.rewards["wall"])
        #print i," ", j , self.grid[i][j].get_policy()


  """
  Method for qlearning logic
  """
  def qlearning(self):
    pos_start = [0,0]
    # Find start position
    for row in range(self.num_row):
      for col in range(self.num_col):
        if self.grid[col][row].get_type() == "start":
          pos_start = [col, row]
    # Start active reinforcement learning
    self.num_steps = 0
    cur_pos = pos_start
    while self.num_steps <= self.max_steps:
      cur_cell = self.grid[cur_pos[0]][cur_pos[1]]
      if cur_cell.get_type() == "goal":
        cur_pos = pos_start
        self.num_steps += 1
        continue
      action = cur_cell.getAction(self.epsilon_val)
      next_pos = [cur_pos[0] + self.letter_to_action(action)[0] , cur_pos[1] + self.letter_to_action(action)[1]]

      #Suppose the grid is surrended by wall
      reward = self.rewards["step"]
      if next_pos[0] < 0 or next_pos[0] >= self.num_row or next_pos[1] < 0 or next_pos[1] >= self.num_col:
        next_pos = cur_pos
        reward = self.rewards["wall"]
      elif self.grid[next_pos[0]][next_pos[1]].get_type() == "wall":
        next_pos = cur_pos
        reward = self.rewards["wall"]  
      next_cell = self.grid[next_pos[0]][next_pos[1]]
      cur_cell.update(action, next_cell, reward, self.learning_rate, self.discount_factor, self.weight)
      cur_pos = next_pos
      self.num_steps += 1


    # return the final policy map
    self.policy_map = []
    for i in range(self.num_row):
      policy_list = [] 
      for j in range(self.num_col):      
        if self.grid[i][j].get_type() == "goal":
          policy_list.append("GOAL")
        elif self.grid[i][j].get_type() == "block":
          policy_list.append("BLOCK")
        else:
          policy_list.append(self.grid[i][j].get_policy())
      self.policy_map.append(policy_list)

    target = open("policy_list","wb")
    target.write("%s" %self.policy_map)
    target.close()

    target = open("path_list","wb")
    for path in self.policy_map:
      for p in path:
        target.write("%6s " %p)
      target.write('\n')
    target.close()

    return self.policy_map



  """
  Helper method
  """
  def letter_to_action(self,action):
    direction = []
    if action == "N":
      direction = [-1,0]
    elif action == "S":
      direction = [1,0]
    elif action == "W":
      direction = [0,-1]
    else:
      direction = [0,1]
    return direction


  """
  Update cell
  """
  def update_grid(self, grid):
    for i in range(self.num_row):
      for j in range(self.num_col):
        self.grid[i][j] = self.Cell("free", 0)
        if grid[i][j] == "start":
          self.grid[i][j] = self.Cell("start",0)
        if grid[i][j] == "goal":
          self.grid[i][j] = self.Cell("goal", self.rewards["goal"])
        if grid[i][j] == "block":
          self.grid[i][j] = self.Cell("block", self.rewards["block"])
        #print i," ", j , self.grid[i][j].get_policy()

