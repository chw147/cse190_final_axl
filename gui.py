"""
This program is for the GUI of our cse190 final project.
Using pygame to create window and display grid.
"""

import pygame
import os
from read_config import read_config
import threading
from qlearning import *
import copy
 
class GUIMAP():

  def __init__(self):
    # Define some colors
    BLACK = (119, 136, 153)
    WHITE = (255, 255, 255)
    GREEN = (0, 255, 0)
    RED = (255, 0, 0)
     
    # This sets the WIDTH and HEIGHT of each grid location
    WIDTH = 40
    HEIGHT = 40
     
    # This sets the margin between each cell
    MARGIN = 10
     
    # initialize variables
    self.config = read_config()
    self.num_row = self.config["map_size"][0]
    self.num_col = self.config["map_size"][1]
    self.pos_start = self.config["start"]
    self.pos_hydrant = self.config["hydrants"]
  
    # initialize grid
    self.grid = [["free" for col in range(self.num_col)] for row in range(self.num_row)]
    for i in range(self.num_row):
      for j in range(self.num_col):
        if [i, j] == self.pos_start:
          self.grid[i][j] = "truck"
        elif [i, j] in self.pos_hydrant:
          self.grid[i][j] = "hydrant"


    # Initialize pygame
    pygame.init()

    # Set the HEIGHT and WIDTH of the screen
    # TO DO
    WINDOW_SIZE = [510, 510]
    screen = pygame.display.set_mode(WINDOW_SIZE)
     
    # Set title of screen
    pygame.display.set_caption("CSE190 Final Project")
     
    # Used to manage how fast the screen updates
    clock = pygame.time.Clock()

    # Loop until the user clicks the close button.
    done = False

    # Read fire truck image
    fireTruckImage = pygame.image.load(os.path.abspath("fire-truck.png"))
    fireTruckImage = pygame.transform.scale(fireTruckImage, (WIDTH, HEIGHT))
    fireTruckImage = fireTruckImage.convert_alpha()

    # Read hydrant image
    hydrantImage = pygame.image.load(os.path.abspath("hydrant.png"))
    hydrantImage = pygame.transform.scale(hydrantImage, (WIDTH, HEIGHT))
    hydrantImage = hydrantImage.convert_alpha()

    # Read block image
    blockImage = pygame.image.load(os.path.abspath("wall_red.png"))
    blockImage = pygame.transform.scale(blockImage, (WIDTH, HEIGHT))
    blockImage = blockImage.convert_alpha()

    # initialize position lists
    self.pos_truck = []
    self.pos_hydrant = []
    self.pos_block = []
    self.pos_fire = []
    self.path = []

    # initialize control variable
    self.isHasWater = False
    index = 0

    # initialze qlearning instance
    self.qlearning = qlearning()



     
    # -------- Main Program Loop -----------
    while not done:
      for event in pygame.event.get():  # User did something
        if event.type == pygame.QUIT:  # If user clicked close
          done = True
     
      fire_pos = 0
      fire_block = 0
      # Set the screen background
      screen.fill(BLACK)

      # apply qlearning on current grid if truck has finished the path
      if len(self.path) == 0:
        index = 0
      
        # set new fire and block if current fire is put out
        if not self.isHasWater:
          self.pos_fire = []
          self.pos_block = []
          # clear current fire and block
          for row in range(self.num_row):
            for col in range(self.num_col):
              if self.grid[row][col] == "fire":
                self.grid[row][col] = "free"
              elif self.grid[row][col] == "block":
                self.grid[row][col] = "free"
          fire_pos = self.randFire()
          print "fire at ", fire_pos
          block_pos = self.randBlock()

        # prepare data for gui and qlearning
        tempMap = copy.deepcopy(self.grid)
        start = []
        goal = []
        for row in range(self.num_row):
          for col in range(self.num_col):
            if self.grid[row][col] == "truck":
              self.pos_truck = [row, col]
              tempMap[row][col] = "start"
              start = [row, col]
            elif self.grid[row][col] == "fire":
              self.pos_fire.append([row, col])
              if self.isHasWater:
                #tempMap[row][col] = "goal"
                goal.append([row, col])
            elif self.grid[row][col] == "block":
              self.pos_block.append([row, col]) 
            elif self.grid[row][col] == "hydrant":
              self.pos_hydrant.append([row, col])
              if not self.isHasWater:
                #tempMap[row][col] = "goal"
                goal.append([row, col])


        self.path = [[-1, -1] for i in range(1,100)]

        # temporarily hide hydrants and fire
        for row in range(self.num_row):
          for col in range(self.num_col):
            if tempMap[row][col] == "hydrant" or tempMap[row][col] == "fire":
              tempMap[row][col] = "free"
        # do qlearning for goals one at a time
        print "Position of Hydrants " ,goal
        for pos_goal in goal:
          p = []
          tempMap_Goal = copy.deepcopy(tempMap)
          tempMap_Goal[pos_goal[0]][pos_goal[1]] = "goal"
          self.qlearning.update_grid(tempMap_Goal)
          #print "start = ", start
          #print "goal =", pos_goal
          #print tempMap_Goal
          p.append(start)
          ql = self.qlearning.qlearning()
          t_to_h = self.getPath(ql, start, pos_goal,fire_pos,p)
          temp = len(t_to_h)

          print "from start ",  start, " to hydrant " , pos_goal, " takes " ,len(t_to_h), "steps"

          tempMap_Goal[pos_goal[0]][pos_goal[1]] = "free"
          tempMap_Goal[fire_pos[0]][fire_pos[1]] = "goal"
          self.qlearning.update_grid(tempMap_Goal)
          ql = self.qlearning.qlearning()
          h_to_f = self.getPath(ql,pos_goal, fire_pos,fire_pos,p)
          print "from hydrant ",  pos_goal, " to fire " , fire_pos, " takes " ,abs(len(h_to_f)-temp), "steps\n"

          if len(self.path) >= len(h_to_f):
            self.path = h_to_f
            for row in range(self.num_row):
              for col in range(self.num_col):
                if self.grid[row][col] == "truck":
                  self.grid[row][col] = "free"
            self.grid[fire_pos[0]][fire_pos[1]] = "truck"
          
      keyboard = pygame.key.get_pressed()
      if keyboard[pygame.K_RETURN]:
        index = index + 1
      if index == len(self.path):
        self.path = []
        continue
      self.pos_truck = self.path[index]
      #print self.pos_truck

      # Draw the grid
      for row in range(self.num_row):
        for col in range(self.num_col):
          color = WHITE
          if [row, col] in self.pos_fire:
            color = RED
          if [row, col] == self.pos_truck:
            screen.blit(fireTruckImage, ((MARGIN+WIDTH)*col+MARGIN, (MARGIN+HEIGHT)*row+MARGIN))
          elif [row, col] in self.pos_hydrant:
            screen.blit(hydrantImage, ((MARGIN+WIDTH)*col+MARGIN, (MARGIN+HEIGHT)*row+MARGIN))
          elif [row, col] in self.pos_block:
            screen.blit(blockImage, ((MARGIN+WIDTH)*col+MARGIN, (MARGIN+HEIGHT)*row+MARGIN))
          else:
            pygame.draw.rect(screen,
                              color,
                              [(MARGIN + WIDTH) * col + MARGIN,
                              (MARGIN + HEIGHT) * row + MARGIN,
                              WIDTH,
                              HEIGHT])
     
      for l in self.path:
        if l != self.pos_truck and l != self.path[len(self.path)-1]:
          if l != self.pos_hydrant[0] and l != self.pos_hydrant[1]:
            pygame.draw.rect(screen, GREEN, [(MARGIN + WIDTH) * l[1] + MARGIN,
                              (MARGIN + HEIGHT) * l[0] + MARGIN,
                              WIDTH,
                              HEIGHT])
                  
   
      # Limit to 60 frames per second
      clock.tick(10)
   
      # Go ahead and update the screen with what we've drawn.
      pygame.display.flip()
     
    # Be IDLE friendly. If you forget this line, the program will 'hang'
    # on exit.
    pygame.quit()






  def randCell(self):
    y = random.randint(0, self.num_col - 1)
    x = random.randint(0, self.num_row - 1)
    return [x, y]

  def randFire(self):
    tempCell = self.randCell()
    # random a position for fire
    while self.grid[tempCell[0]][tempCell[1]] != "free":
      tempCell = self.randCell()
    
    #self.grid[tempCell[0]][tempCell[1]] = "fire"
    self.grid[tempCell[0]][tempCell[1]] = "fire"
    return tempCell

  def randBlock(self):
    
    counter = 0
    while counter < 20:
      counter += 1
      tempCell = self.randCell()
      while self.grid[tempCell[0]][tempCell[1]] != "free":
        tempCell = self.randCell()

      self.grid[tempCell[0]][tempCell[1]] = "block"
    return tempCell




  def getPath(self, policy, start, goal,fire,path):
    currPos = start
    currPolicy = ""

    while currPos != goal:
      currPolicy = policy[currPos[0]][currPos[1]]
      if currPolicy == "W":
        currPos = [currPos[0], currPos[1]-1]
      elif currPolicy == "E":
        currPos = [currPos[0], currPos[1]+1]
      elif currPolicy == "N":
        currPos = [currPos[0]-1, currPos[1]]
      elif currPolicy == "S":
        currPos = [currPos[0]+1, currPos[1]]
      path.append(currPos)
    return path




if __name__ == '__main__':
  gui = GUIMAP()
